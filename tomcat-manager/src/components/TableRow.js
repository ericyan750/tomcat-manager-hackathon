import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class TableRow extends Component {
    constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
    }
    delete() {
        axios.delete('http://localhost:3005/deleteLibrary/'+this.props.obj.orgid)
            .then(console.log('Deleted'))
            .catch(err => console.log(err))

        setTimeout(function(){
          window.location.pathname = '/index';
        }, 200);
    }
  render() {
    return (
        <tr>
          <td>
            {this.props.obj.orgid}
          </td>
          <td>
            {this.props.obj.organization}
          </td>
          <td>
            {this.props.obj.host}
          </td>
          <td>
            {this.props.obj.ils_type}
          </td>
          <td>
            {this.props.obj.tomcat_manager_username}
          </td>
          <td>
            {this.props.obj.tomcat_manager_password}
          </td>
          <td>
            {this.props.obj.tomcat_version}
          </td>
          <td>
            <Link to={"/edit/"+this.props.obj.orgid} className="btn btn-primary">Edit</Link>
          </td>
          <td>
            <button onClick={this.delete} className="btn btn-danger">Delete</button>
          </td>
        </tr>
    );
  }
}
export default TableRow;
