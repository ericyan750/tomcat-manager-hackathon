// create.component.js

import React, { Component } from 'react';
import axios from 'axios';

export default class Create extends Component {
  constructor(props) {
      super(props);
      this.onChangeOrgID = this.onChangeOrgID.bind(this);
      this.onChangeOrganization = this.onChangeOrganization.bind(this);
      this.onChangeHost = this.onChangeHost.bind(this);
      this.onChangePort = this.onChangePort.bind(this);
      this.onChangeIlsType = this.onChangeIlsType.bind(this);
      this.onChangeTomcatManagerUsername = this.onChangeTomcatManagerUsername.bind(this);
      this.onChangeTomcatManagerPassword = this.onChangeTomcatManagerPassword.bind(this);
      this.onChangeNotes = this.onChangeNotes.bind(this);
      this.onChangeOperatingSystem = this.onChangeOperatingSystem.bind(this);
      this.onChangeSSHLogin = this.onChangeSSHLogin.bind(this);
      this.onChangeSSHPassword = this.onChangeSSHPassword.bind(this);
      this.onChangeTomcatPath = this.onChangeTomcatPath.bind(this);
      this.onChangeTomcatVersion = this.onChangeTomcatVersion.bind(this);
      this.onChangeJVMVersion = this.onChangeJVMVersion.bind(this);
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
          orgid: '',
          organization: '',
          host: '',
          port: '',
          ils_type: '',
          tomcat_manager_username: '',
          tomcat_manager_password: '',
          notes: '',
          operating_system: '',
          ssh_login: '',
          ssh_password: '',
          tomcat_path: '',
          tomcat_version: '',
          jvm_version: ''
      }
  }

  onChangeOrgID(e) {
    this.setState({
      orgid: e.target.value
    });
  }
  onChangeOrganization(e) {
    this.setState({
      organization: e.target.value
    })
  }
  onChangeHost(e) {
    this.setState({
      host: e.target.value
    })
  }
  onChangePort(e) {
    this.setState({
      port: e.target.value
    });
  }
  onChangeIlsType(e) {
    this.setState({
      ils_type: e.target.value
    })
  }
  onChangeTomcatManagerUsername(e) {
    this.setState({
      tomcat_manager_username: e.target.value
    })
  }
  onChangeTomcatManagerPassword(e) {
    this.setState({
      tomcat_manager_password: e.target.value
    });
  }
  onChangeNotes(e) {
    this.setState({
      notes: e.target.value
    })
  }
  onChangeOperatingSystem(e) {
    this.setState({
      operating_system: e.target.value
    })
  }
  onChangeSSHLogin(e) {
    this.setState({
      ssh_login: e.target.value
    });
  }
  onChangeSSHPassword(e) {
    this.setState({
      ssh_password: e.target.value
    })
  }
  onChangeTomcatPath(e) {
    this.setState({
      tomcat_path: e.target.value
    })
  }
  onChangeTomcatVersion(e) {
    this.setState({
      tomcat_version: e.target.value
    });
  }
  onChangeJVMVersion(e) {
    this.setState({
      jvm_version: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();
    console.log(`The values are ${this.state.orgid}, ${this.state.organization}, ${this.state.host}, ${this.state.port}, ${this.state.ils_type}, ${this.state.tomcat_manager_username}, ${this.state.tomcat_manager_password}, ${this.state.notes}, ${this.state.operating_system}, ${this.state.ssh_login}, ${this.state.ssh_password}, ${this.state.tomcat_path}, ${this.state.tomcat_version}, ${this.state.jvm_version} `)
    const obj = {
      orgid: this.state.orgid,
      organization: this.state.organization,
      host: this.state.host,
      port: this.state.port,
      ils_type: this.state.ils_type,
      tomcat_manager_username: this.state.tomcat_manager_username,
      tomcat_manager_password: this.state.tomcat_manager_password,
      notes: this.state.notes,
      operating_system: this.state.operating_system,
      ssh_login: this.state.ssh_login,
      ssh_password: this.state.ssh_password,
      tomcat_path: this.state.tomcat_path,
      tomcat_version: this.state.tomcat_version,
      jvm_version: this.state.jvm_version
    };
    axios.post('http://localhost:3005/createLibrary', obj)
        .then(response => console.log(response.data));
    setTimeout(function(){
      window.location.pathname = '/index';
    }, 200);
  }
i
  render() {
      return (
          <div style={{ marginTop: 10 }}>
              <h3>Add New Tomcat Manager</h3>
              <form onSubmit={this.onSubmit}>
                  <div className="form-group">
                      <label>Agency/Org ID: </label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.orgid}
                        onChange={this.onChangeOrgID}
                        />
                  </div>
                  <div className="form-group">
                      <label>Organization: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.organization}
                        onChange={this.onChangeOrganization}
                        />
                  </div>
                  <div className="form-group">
                      <label>Host: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.host}
                        onChange={this.onChangeHost}
                        />
                  </div>
                  <div className="form-group">
                      <label>Port: </label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.port}
                        onChange={this.onChangePort}
                        />
                  </div>
                  <div className="form-group">
                      <label>ILS Type: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.ils_type}
                        onChange={this.onChangeIlsType}
                        />
                  </div>
                  <div className="form-group">
                      <label>Tomcat Manager Username: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.tomcat_manager_username}
                        onChange={this.onChangeTomcatManagerUsername}
                        />
                  </div>
                  <div className="form-group">
                      <label>Tomcat Manager Password: </label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.tomcat_manager_password}
                        onChange={this.onChangeTomcatManagerPassword}
                        />
                  </div>
                  <div className="form-group">
                      <label>Notes: </label>
                      <textarea rows="5"
                        className="form-control"
                        value={this.state.notes}
                        onChange={this.onChangeNotes}
                        />
                  </div>
                  <div className="form-group">
                      <label>Operating System: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.operating_system}
                        onChange={this.onChangeOperatingSystem}
                        />
                  </div>
                  <div className="form-group">
                      <label>SSH User: </label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.ssh_login}
                        onChange={this.onChangeSSHLogin}
                        />
                  </div>
                  <div className="form-group">
                      <label>SSH Password: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.ssh_password}
                        onChange={this.onChangeSSHPassword}
                        />
                  </div>
                  <div className="form-group">
                      <label>Tomcat Path: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.tomcat_path}
                        onChange={this.onChangeTomcatPath}
                        />
                  </div>
                  <div className="form-group">
                      <label>Tomcat Version: </label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.tomcat_version}
                        onChange={this.onChangeTomcatVersion}
                        />
                  </div>
                  <div className="form-group">
                      <label>JVM Version: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.jvm_version}
                        onChange={this.onChangeJVMVersion}
                        />
                  </div>
                  <div className="form-group">
                      <input type="submit" value="Add Tomcat Manager" className="btn btn-primary"/>
                  </div>
              </form>
          </div>
      )
  }
}



