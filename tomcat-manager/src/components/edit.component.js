// edit.component.js

import React, { Component } from 'react';
import axios from 'axios';

export default class Edit extends Component {
  constructor(props) {
    super(props);
    this.onChangeOrganization = this.onChangeOrganization.bind(this);
    this.onChangeHost = this.onChangeHost.bind(this);
    this.onChangeOrgId = this.onChangeOrgId.bind(this);
    this.onChangePort = this.onChangePort.bind(this);
    this.onChangeIlsType = this.onChangeIlsType.bind(this);
    this.onChangeTomcatManagerUsername = this.onChangeTomcatManagerUsername.bind(this);
    this.onChangeTomcatManagerPassword = this.onChangeTomcatManagerPassword.bind(this);
    this.onChangeNotes = this.onChangeNotes.bind(this);
    this.onChangeOperatingSystem = this.onChangeOperatingSystem.bind(this);
    this.onChangeSshLogin = this.onChangeSshLogin.bind(this);
    this.onChangeSshPassword = this.onChangeSshPassword.bind(this);
    this.onChangeTomcatPath = this.onChangeTomcatPath.bind(this);
    this.onChangeTomcatVersion = this.onChangeTomcatVersion.bind(this);
    this.onChangeJvmVersion = this.onChangeJvmVersion.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      orgid: '',
      organization: '',
      host:'',
      port:'',
      ilsType:'',
      tomcatManagerUsername:'',
      tomcatManagerPassword:'',
      notes:'',
      operatingSystem:'',
      sshLogin:'',
      sshPassword:'',
      tomcatPath:'',
      tomcatVersion:'',
      jvmVersion:''
    }
  }

  componentDidMount() {
    axios.get('http://localhost:3005/getlibrarybyorgid/'+this.props.match.params.orgid)
        .then(response => {
            console.log(response.data);
            this.setState({
              orgid: response.data[0].orgid,
              organization: response.data[0].organization,
              host: response.data[0].host,
              port: response.data[0].port,
              ilsType: response.data[0].ils_type,
              tomcatManagerUsername: response.data[0].tomcat_manager_username,
              tomcatManagerPassword: response.data[0].tomcat_manager_password,
              notes: response.data[0].notes,
              operatingSystem: response.data[0].operating_system,
              sshLogin: response.data[0].ssh_login,
              sshPassword: response.data[0].ssh_password,
              tomcatPath: response.data[0].tomcat_path,
              tomcatVersion: response.data[0].tomcat_version,
              jvmVersion: response.data[0].jvm_version});
        })
        .catch(function (error) {
            console.log(error);
        })
  }

  onChangeOrgId(e) {
    this.setState({
      orgid: e.target.value
    });
  }
  onChangeOrganization(e) {
    this.setState({
      organization: e.target.value
    })
  }
  onChangeHost(e) {
    this.setState({
      host: e.target.value
    })
  }
  onChangePort(e) {
    this.setState({
      port: e.target.value
    })
  }
  onChangeIlsType(e) {
    this.setState({
      ilsType: e.target.value
    })
  }
  onChangeTomcatManagerUsername(e) {
    this.setState({
      tomcatManagerUsername: e.target.value
    })
  }
  onChangeTomcatManagerPassword(e) {
    this.setState({
      tomcatManagerPassword: e.target.value
    })
  }
  onChangeNotes(e) {
    this.setState({
      notes: e.target.value
    })
  }
  onChangeOperatingSystem(e) {
    this.setState({
      operatingSystem: e.target.value
    })
  }
  onChangeSshLogin(e) {
    this.setState({
      sshLogin: e.target.value
    })
  }
  onChangeSshPassword(e) {
    this.setState({
      sshPassword: e.target.value
    })
  }
  onChangeTomcatPath(e) {
    this.setState({
      tomcatPath: e.target.value
    })
  }
  onChangeTomcatVersion(e) {
    this.setState({
      tomcatVersion: e.target.value
    })
  }
  onChangeJvmVersion(e) {
    this.setState({
      jvmVersion: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
      organization: this.state.organization,
      host: this.state.host,
      port: this.state.port,
      ils_type: this.state.ilsType,
      tomcat_manager_username: this.state.tomcatManagerUsername,
      tomcat_manager_password: this.state.tomcatManagerPassword,
      notes: this.state.notes,
      operating_system: this.state.operatingSystem,
      ssh_login: this.state.sshLogin,
      ssh_password: this.state.sshPassword,
      tomcat_path: this.state.tomcatPath,
      tomcat_version: this.state.tomcatVersion,
      jvm_version: this.state.jvmVersion
    };
    axios.put('http://localhost:3005/updateLibrary/'+this.props.match.params.orgid, obj)
        .then(res => console.log(res.data));

    setTimeout(function(){
      window.location.pathname = '/index';
    }, 200);
  }

   render() {
    return (
        <div style={{ marginTop: 10 }}>
            <h3 align="center">Update Library</h3>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Org Id:  </label>
                    <input
                      type="text"
                      className="form-control"
                      value={this.state.orgid}
                      onChange={this.onChangeOrgId}
                      />
                </div>
                <div className="form-group">
                    <label>Organization: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.organization}
                      onChange={this.onChangeOrganization}
                      />
                </div>
                <div className="form-group">
                    <label>Host: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.host}
                      onChange={this.onChangeHost}
                      />
                </div>
                <div className="form-group">
                    <label>Port: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.port}
                      onChange={this.onChangePort}
                      />
                </div>
                <div className="form-group">
                    <label>Ils Type: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.ilsType}
                      onChange={this.onChangeIlsType}
                      />
                </div>
                <div className="form-group">
                    <label>Tomcat Manager Username: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.tomcatManagerUsername}
                      onChange={this.onChangeTomcatManagerUsername}
                      />
                </div>
                <div className="form-group">
                    <label>Tomcat Manager Password: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.tomcatManagerPassword}
                      onChange={this.onChangeTomcatManagerPassword}
                      />
                </div>
                <div className="form-group">
                    <label>Notes </label>
                    <textarea rows="5"
                      className="form-control"
                      value={this.state.notes}
                      onChange={this.onChangeNotes}
                      />
                </div>
                <div className="form-group">
                    <label>Operating System: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.operatingSystem}
                      onChange={this.onChangeOperatingSystem}
                      />
                </div>
                <div className="form-group">
                    <label>Ssh Login: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.sshLogin}
                      onChange={this.onChangeSshLogin}
                      />
                </div>
                <div className="form-group">
                    <label>Ssh Password: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.sshPassword}
                      onChange={this.onChangeSshPassword}
                      />
                </div>
                <div className="form-group">
                    <label>Tomcat Path: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.tomcatPath}
                      onChange={this.onChangeTomcatPath}
                      />
                </div>
                <div className="form-group">
                    <label>Tomcat Version: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.tomcatVersion}
                      onChange={this.onChangeTomcatVersion}
                      />
                </div>
                <div className="form-group">
                    <label>Jvm Version: </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.jvmVersion}
                      onChange={this.onChangeJvmVersion}
                      />
                </div>

                <div className="form-group">
                    <input type="submit"
                      value="Update"
                      className="btn btn-primary"/>
                </div>
            </form>
        </div>
    )
  }
}
