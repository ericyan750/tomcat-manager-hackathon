// index.component.js

import React, { Component } from 'react';
import axios from 'axios';
import TableRow from './TableRow';

export default class Index extends Component {
    constructor(props) {
      super(props);
      this.updateQuery = this.updateQuery.bind(this);
      this.state = {
        libraries: [],
        query: null
      };
    }

    componentDidMount(){
      axios.get('http://localhost:3005/getLibraryTomcats')
        .then(response => {
          this.setState({ libraries: response.data });
        })
        .catch(function (error) {
          console.log(error);
        })
    }
    updateQuery(e){
      this.setState({query: e.target.value});
      console.info(this.state);
    }
    tabRow(){
      return this.state.libraries.filter((object) => {
        if (this.state.query) {
          const matchQuery = new RegExp(this.state.query, 'i');
          return object.orgid.match(matchQuery) || object.organization.match(matchQuery);
        }
        return true;
      })
        .map(function(object, i){
          console.info(object);
          return <TableRow obj={object} key={i} />;
      });
    }

    render() {
      return (
        <div>
          <h3 align="center">Tomcat Manager</h3>
          <input onKeyPress={this.updateQuery} className="search-bar" type="search" />
          <button>Search</button>
          <table className="table table-striped" style={{ marginTop: 20 }}>
            <thead>
              <tr>
                <th>Org ID</th>
                <th>Organization</th>
                <th>Host</th>
                <th>ILS</th>
                <th>Tomcat Username</th>
                <th>Tomcat Password</th>
                <th>Tomcat Version</th>
                <th colSpan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              { this.tabRow() }
            </tbody>
          </table>
        </div>
      );
    }
}
