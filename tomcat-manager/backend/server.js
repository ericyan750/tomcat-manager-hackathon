const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const db = require('./queries')
const port = 3005

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.use(function(req, res, next) {
  res.header("Access-control-Allow-Methods", "POST, PUT, GET, DELETE")
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

app.get('/getLibraryTomcats', db.getLibraryTomcats)
app.get('/getLibraryByOrgId/:orgid', db.getLibraryByOrgId)
app.post('/createLibrary', db.createLibrary)
app.put('/updateLibrary/:orgid', db.updateLibrary)
app.delete('/deleteLibrary/:orgid', db.deleteLibrary)

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})
