const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'tomcatmanager',
  password: '',
  port: 5432,
})

const getLibraryTomcats = (request, response) => {
  pool.query('SELECT * FROM librarytomcats  ORDER BY organization ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getLibraryByOrgId = (request, response) => {
  const orgid = request.params.orgid
  console.log("Getting " + request.params.orgid)

  pool.query('SELECT * FROM librarytomcats WHERE orgid = $1', [orgid], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createLibrary = (request, response) => {
  const { orgid, organization, host, port, ils_type, tomcat_manager_username, tomcat_manager_password, notes, operating_system, ssh_login, ssh_password, tomcat_path, tomcat_version, jvm_version } = request.body

  pool.query('INSERT INTO librarytomcats VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)',
    [orgid, organization, host, port, ils_type, tomcat_manager_username, tomcat_manager_password, notes, operating_system, ssh_login, ssh_password, tomcat_path, tomcat_version, jvm_version],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(201).send(`Library added with orgID: ${orgid}`)
    }
  )
}

const updateLibrary = (request, response) => {
  const orgid = request.params.orgid
  const { organization, host, port, ils_type, tomcat_manager_username, tomcat_manager_password, notes, operating_system, ssh_login, ssh_password, tomcat_path, tomcat_version, jvm_version } = request.body

  pool.query(
    'UPDATE librarytomcats SET organization = $2, host = $3, port = $4, ils_type = $5, tomcat_manager_username = $6, tomcat_manager_password = $7, notes = $8, operating_system = $9, ssh_login = $10, ssh_password = $11, tomcat_path = $12, tomcat_version = $13, jvm_version = $14 WHERE orgid = $1',
    [orgid, organization, host, port, ils_type, tomcat_manager_username, tomcat_manager_password, notes, operating_system, ssh_login, ssh_password, tomcat_path, tomcat_version, jvm_version],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Library modified with orgID: ${orgid}`)
    }
  )
}

const deleteLibrary = (request, response) => {
  const orgid = request.params.orgid
  console.log("Deleting " + orgid)
  pool.query('DELETE FROM librarytomcats WHERE orgid = $1', [orgid], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User library with orgID: ${orgid}`)
  })
}

module.exports = {
  getLibraryTomcats,
  getLibraryByOrgId,
  createLibrary,
  updateLibrary,
  deleteLibrary,
}
